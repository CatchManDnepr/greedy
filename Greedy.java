import java.util.Scanner;

class Greedy{
    
    public static void main(String []argv){
	float money = input();
	int[] arrayCoins = {25,10,5,1};
	System.out.println("You must give " + calckulateSum(money, arrayCoins) + " coins");
    }
    /*
    function for checf string == float
    @param string
    @return true/false
    */
    public static boolean isFloat(String str){
	float num;
	try {
	    num = Float.parseFloat(str);
	} catch(NumberFormatException e) {
	    System.out.println("Don`t correct format!!!");
	    return false;
	} catch(NullPointerException e) {
	    System.out.println("Empty string!!!");
	    return false;
	}
	if(num < 0){
	    System.out.println("Your num mus`t be more then 0!!!");
	    return false;
	}
	return true;
    }
    /*
    function for input data
    @param
    @return corect data
    */
    public static float input(){
	Scanner in = new Scanner(System.in);
	String str;
	do{
	    System.out.println("Input money");
	    str = in.nextLine();
	}while(!isFloat(str));
	return Float.parseFloat(str);
    }
    /*
    function for calk minimum coins whitch you need
    @param count money and array all variants of coins
    @return count coins
    */
    public static int calckulateSum(float money, int[] array){
	money *= 100;
	int dev;
	int count = 0;
	for(int i = 0; i < array.length; i++){
	    dev = ((int)money - (int)money % array[i])/ array[i];
	    count += dev; 
	    System.out.println(array[i] + ":" + dev);
	    money -= dev * array[i];
	}
	return count;
    }
}